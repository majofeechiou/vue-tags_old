'use strict'
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var FILE_SRC = 'src/';
var FILE_BUILD = 'build/';

module.exports = {
    entry: {
        'main': [ './'+FILE_SRC+'main.js'],
        'demo': [ './'+FILE_SRC+'demo.js']
    },
    module: {
        loaders: [
            {
                test: /\.vue$/,
                loader: 'vue'
            },
            {
                test: /\.js$/,
                // excluding some local linked packages.
                // for normal use cases only node_modules is needed.
                exclude: /node_modules|vue\/dist|vue-router\/|vue-loader\/|vue-hot-reload-api\//,
                loader: 'babel'
            },
            { 
                test: /\.js$/, 
                loaders: ['script-loader'], 
                include: /external/
            },
            { 
                test: /\.js$/, 
                loaders: ['babel-loader']
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract(
                    'style-loader',
                    'css-loader',
                    {
                        publicPath: '../',  // 回到dist，像是 background-image: url(../img/header-e147da.jpg); 就是由這控制的
                        // publicPath: path.join(__dirname, 'dist'),  // 絕對路徑是要不得的作法，你可以看你我用筆電看到的結果 -> background-image: url(D:\develop\kkdev\showcase\distimg/header-e147da.jpg);
                    }
                )
            },
            {
                test: /\.css$/,
                loader: 'postcss-loader'
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/, 
                exclude: /(node_modules|bower_components)/,
                loader: "url-loader",
                query: {
                    limit: 10000, // 若大於10kb，就會轉為base64
                    name: 'images/[name].[ext]'  // 這邊的路徑會相對於使用url-loader的loader, 也就是scss的publicPath
                }
            },
            {
                test: /\.woff(2)?$/, 
                loader: "url-loader",
                query: {
                    limit: 10000,
                    name: 'font/[name].[ext]'
                }
            },
            {
                test: /\.(ttf|eot|svg)$/, 
                loader: "file-loader",
                query: {
                    name: 'font/[name].[ext]'
                }
            }
        ]
    },
    output: {
        path: path.join(__dirname, FILE_BUILD),
        // publicPath: path.join(__dirname, FILE_BUILD),
        // filename: 'js/index.js'
        filename: 'js/[name].js'
    },
    resolve: {
        extensions: ['', '.js', '.css', '.vue'],
        alias: {
            'vue': path.join(__dirname, 'lib/vue/vue.js'),
            // 'utilsCssUtils': path.join(__dirname, 'node_modules/utils-css/src/utils.css'),
            // 'utilsCssStyle': path.join(__dirname, 'node_modules/utils-css/src/style.css')
        }
    },
    postcss: function () {
        return [ require('precss'), require('autoprefixer'), require('postcss-fontpath'), require("postcss-extend"), require("postcss-calc"), require("postcss-color") ];
    },
    plugins: [
        new ExtractTextPlugin('css/[name].css')
    ]
}
